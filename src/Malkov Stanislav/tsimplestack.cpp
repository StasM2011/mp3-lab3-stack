#include <iostream>
#include "tsimplestack.h"

using namespace std;

TSimpleStack::~TSimpleStack() {
	delete[] pMem;
}

bool TSimpleStack::IsEmpty() const {
	return DataCount == 0;
}			

bool TSimpleStack::IsFull() const {
	return DataCount == MemSize;
}

void TSimpleStack::Put(const TData& val) {
    if(IsFull()) {
		SetRetCode(DataFull);
        throw exception("is full");
	}
	pMem[DataCount] = val;
	DataCount++;
	SetRetCode(DataOK);
}

TData TSimpleStack::Get() {
    if(IsEmpty()) {
		SetRetCode(DataEmpty);
        throw exception("is empty");
	}
	DataCount--;
	SetRetCode(DataOK);
	return pMem[DataCount + 1];
}

int TSimpleStack::IsValid() {
	return RetCode != DataErr;
}

void TSimpleStack::Print() {
	std::cout << DataCount << std::endl;
	for(int i = 0; i < DataCount; i++)
		std::cout << pMem[i] << " ";
}