#include <iostream>
#include "tstack.h"

using namespace std;

TStack::~TStack() {
	delete[] pMem;
}

bool TStack::IsEmpty() const {
	return DataCount == 0;
}			

bool TStack::IsFull() const {
	return DataCount == MemSize;
}

void TStack::Put(const TData& val) {
    if(IsFull()) {
		SetRetCode(DataFull);
		return;
	}
	pMem[DataCount] = val;
	SetMem(new PTElem, ++DataCount);
	SetRetCode(DataOK);
}

TData TStack::Get() {
    if(IsEmpty()) {
		SetRetCode(DataEmpty);
        return 0;
	}
	SetMem(new PTElem, DataCount--);
	SetRetCode(DataOK);
	return pMem[DataCount];
}

int TStack::IsValid() {
	return RetCode != DataErr;
}

void TStack::Print() {
	std::cout << DataCount << std::endl;
	for(int i = 0; i < DataCount; i++)
		std::cout << pMem[i] << " ";
}